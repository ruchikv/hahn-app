﻿using FluentValidation;
using Hahn.ApplicationProcess.May2020.Domain.Models;
using RestSharp;
using System;

namespace Hahn.ApplicationProcess.May2020.Domain.Validators
{
    public class ApplicantValidator : AbstractValidator<Applicant>
    {
        public ApplicantValidator()
        {
            RuleFor(applicant => applicant.Name).MinimumLength(5);
            RuleFor(applicant => applicant.FamilyName).MinimumLength(5);
            RuleFor(applicant => applicant.Address).MinimumLength(10);
            RuleFor(applicant => applicant.CountryOfOrigin).Custom((country, context) => {
                var client = new RestClient("https://restcountries.eu/rest/v2/");
                var request = new RestRequest($"name/{country}?fullText=true", Method.GET);
                var response = client.Execute(request);
                if (!response.IsSuccessful)
                {
                    context.AddFailure($"{country} is not a valid country");
                }
            });
            RuleFor(applicant => applicant.EMailAddress).Matches(@"^[^@\s]+@[^@\s\.]+\.[^@\.\s]+$");
            RuleFor(applicant => applicant.Age).Must(age => age >= 20 && age <= 60).WithMessage("Age must be between 20 and 60");
            RuleFor(applicant => applicant.Hired).Null();
        }
    }
}
