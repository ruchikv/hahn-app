import { bindable, inject, NewInstance, observable, computedFrom  } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { ValidationController, ValidationRules } from 'aurelia-validation';
import { Applicant } from './applicant';
import { HttpClient } from 'aurelia-fetch-client';
import { BootstrapFormRenderer } from '../renderers/bootstrap-form-renderer';
import { I18N } from 'aurelia-i18n';
import { DialogService } from 'aurelia-dialog';
import { Prompt } from '../prompt/prompt';

@inject(HttpClient, NewInstance.of(ValidationController), Router, DialogService, I18N)
export class ApplicantForm {

	validCountries: string[] = [];
	formValid: boolean;

	@bindable
	applicant: Applicant;
	private httpClient: HttpClient;
	public controller: ValidationController;
	private router: Router;
	private i18n: I18N;
	private dialogService: DialogService;

	constructor(http: HttpClient, controller: ValidationController, router: Router, dialogService: DialogService, i18n: I18N) {
		this.httpClient = http;
		this.controller = controller;
		this.router = router;
		this.dialogService = dialogService;
		this.i18n = i18n;
		this.i18n
			.setLocale('en').then(response => {
			});
		this.controller.addRenderer(new BootstrapFormRenderer());
		this.formValid = false;
		this.applicant = new Applicant(controller);

		this.httpClient.fetch('https://restcountries.eu/rest/v2/all')
			.then(response => response.json())
			.then(data => {
				this.validCountries = data.map(c => c.name);
			});
	}

	addApplicant() {
		this.controller.validate().then(result => {
			let submission = this.router.routes.find(c => c.name == 'submission');
			submission.value = this.applicant;
			this.router.navigateToRoute('submission');
		});
	}

	resetForm() {
		this.dialogService.open({ viewModel: Prompt, model: { title: 'Are you sure?', action: () => this.resetApplicant(), showCancel: true } }).then(response => {
			
		});
	}

	resetApplicant() {
		this.applicant = new Applicant(this.controller);
		document.querySelectorAll('.is-dirty').forEach(item => item.classList.remove('is-dirty'));
		document.querySelectorAll('.is-invalid').forEach(item => item.classList.remove('is-invalid'));
		document.querySelectorAll('.invalid-feedback').forEach(item => item.remove());

	}
}
