﻿import { ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';
import { observable } from 'aurelia-framework';

	@observable({ name: 'name', changeHandler: 'changeHandler' })
	@observable({ name: 'familyName', changeHandler: 'changeHandler' })
	@observable({ name: 'address', changeHandler: 'changeHandler' })
	@observable({ name: 'countryOfOrigin', changeHandler: 'changeHandler' })
	@observable({ name: 'eMailAddress', changeHandler: 'changeHandler' })
	@observable({ name: 'age', changeHandler: 'changeHandler' })
	export class Applicant {

		controller: ValidationController = null;

		constructor(controller: ValidationController) {
			this.controller = controller;
			this.isValid = false;
		}

		ID: number;
		name:string;
		familyName: string;
		address: string;
		countryOfOrigin: string;
		eMailAddress: string;
		age: number;
		hired?: boolean;

		private isValid: boolean;

		changeHandler(newValue, oldValue) {
			this.controller.validate().then(result => this.isValid = result.valid);
		}

		get isModelValid() {
			return this.isValid;
		}
	}

ValidationRules
	.ensure('name').required().minLength(5).withMessage("Name is required and should be minimum 5 characters")
	.ensure('familyName').required().minLength(5).withMessage("Family Name is required and should be minimum 5 characters")
	.ensure('address').required().minLength(10).withMessage("Address is required and should be minimum 10 characters")
	.ensure('countryOfOrigin').required().withMessage("Country of Origin is required")
	.ensure('eMailAddress').required().matches(/^[^@\s]+@[^@\s\.]+\.[^@\.\s]+$/).withMessage("E-Mail Address is required and should be in a valid format")
	.ensure('age').required().min(20).max(60).withMessage("Age is required and should be between 20 and 60 years")
	.on(Applicant);

