import { inject  } from 'aurelia-framework';
import { Applicant } from "../applicant-form/applicant";
import { HttpClient } from 'aurelia-fetch-client';
import { DialogService } from 'aurelia-dialog';
import { Prompt } from '../prompt/prompt';
import { Router } from 'aurelia-router';

@inject(HttpClient, DialogService, Router)
export class Counter {
	applicant: Applicant;
	httpClient: HttpClient;
	dialogService: DialogService;
	router: Router;

	constructor(http: HttpClient, dialogService: DialogService, router: Router) {
		this.httpClient = http;
		this.dialogService = dialogService;
		this.router = router;
	}

	activate(params, routeData) {
		this.applicant = routeData.value;
	}

	submit() {
		this.httpClient.post('/api/applicants', JSON.stringify(<Applicant>{
			name: this.applicant.name,
			familyName: this.applicant.familyName,
			address: this.applicant.address,
			countryOfOrigin: this.applicant.countryOfOrigin,
			eMailAddress: this.applicant.eMailAddress,
			age: this.applicant.age
		})).then(response => {
			if (response.status == 201) {
				return this.dialogService.open({ viewModel: Prompt, model: { title: 'Submitted Successfully!', action: () => this.backToForm() } }).then(response => {

				});
			} else if (response.status == 400) {
				return response.json();
			}
		}).then(response => {
			if (response instanceof Array) {
				return this.dialogService.open({ viewModel: Prompt, model: { title: 'Error!', message: response[0].errorMessage, action: () => this.backToForm() } }).then(response => {

				});
			}
		});
	}

	backToForm() {
		this.router.navigateToRoute('form');
	}
}
