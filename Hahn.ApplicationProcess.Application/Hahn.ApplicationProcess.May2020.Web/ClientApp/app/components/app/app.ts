import { Aurelia, PLATFORM } from "aurelia-framework";
import { Router, RouterConfiguration } from "aurelia-router";

export class App {
	configureRouter(config: RouterConfiguration, router: Router) {
		config.title = "AureliaDotnetTemplate";
		config.map([{
				route: ["", "form"],
				name: "form",
				settings: { icon: "fa fa-plus" },
				moduleId: "../applicant-form/applicant-form",
				nav: true,
				title: "Applicant-Form"
		}, {
				route: "submission",
				name: "submission",
				settings: { icon: "fa fa-check" },
				moduleId: "../applicant-form-submission/applicant-form-submission",
				nav: true,
				title: "Applicant-Form-Submission"
		}]);
	}
}
