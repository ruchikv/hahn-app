using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FluentValidation.Results;
using Hahn.ApplicationProcess.May2020.Data;
using Hahn.ApplicationProcess.May2020.Domain.Models;
using Hahn.ApplicationProcess.May2020.Domain.Validators;
using Hahn.ApplicationProcess.May2020.Web.Examples;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog.Core;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;

namespace Hahn.ApplicationProcess.May2020.Web.Controllers {
	[Route("api/[controller]")]
	[ApiController]
	public class ApplicantsController : Controller {

		readonly AppDbContext db;
		readonly Logger logger;
		public ApplicantsController(AppDbContext db, Logger logger) 
		{
			this.db = db;
			this.logger = logger;
		}

		[HttpPost]
		[SwaggerRequestExample(typeof(Applicant), typeof(ApplicantExample))]
		[SwaggerResponse((int)HttpStatusCode.Created)]
		[SwaggerResponse((int)HttpStatusCode.BadRequest, Type = typeof(IList<ValidationFailure>), Description = "An invalid or missing input parameter will result in a bad request")]
		public async Task<IActionResult> PostApplicant(Applicant applicant) 
		{
			ApplicantValidator validator = new ApplicantValidator();
			ValidationResult result = validator.Validate(applicant);

			if (result.IsValid) {
				db.Applicants.Add(applicant);
				await db.SaveChangesAsync();
				this.logger.Debug($"Added Applicant: {JsonConvert.SerializeObject(applicant)}");
				return CreatedAtAction(nameof(GetApplicant), new { id = applicant.ID }, applicant);
			}
			this.logger.Information($"Invalid Applicant: {JsonConvert.SerializeObject(applicant)}");
			return BadRequest(result.Errors);
		}

		[HttpGet]
		public async Task<IActionResult> GetApplicants(int page = 1, int pageSize = 100) 
		{
			if (page < 1) {
				page = 0;
			}

			return Ok(new {
				Data = await db.Applicants.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync(),
				Count = await db.Applicants.LongCountAsync()
			});
		}

		[HttpGet("{id}", Name = "GetApplicant")]
		public async Task<ActionResult> GetApplicant(int id) 
		{
			var applicant = await db.Applicants.FindAsync(id);

			if (applicant == null) {
				return NotFound();
			}

			return Ok(applicant);
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> PutApplicant(int id, Applicant applicant) {
			if (id != applicant.ID) {
				return BadRequest();
			}

			ApplicantValidator validator = new ApplicantValidator();
			ValidationResult result = validator.Validate(applicant);

			if (result.IsValid) {
				db.Entry(applicant).State = EntityState.Modified;

				try {
					await db.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException ex) {
					if (!await ApplicantExists(id)) {
						return NotFound();
					}
					else {
						return StatusCode(StatusCodes.Status500InternalServerError, ex);
					}
				}
				return NoContent();
			}

			return BadRequest(result.Errors);
		}

		[HttpDelete("{id}")]
		public async Task<ActionResult> DeleteApplicant(int id) 
		{
			var applicant = await db.Applicants.FindAsync(id);
			if (applicant == null) {
				return NotFound();
			}

			db.Applicants.Remove(applicant);
			await db.SaveChangesAsync();

			return Ok(applicant);
		}

		private async Task<bool> ApplicantExists(int id) 
		{
			return await db.Applicants.AnyAsync(c => c.ID == id);
		}
	}
}
