﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hahn.ApplicationProcess.May2020.Domain.Models;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.Swagger;

namespace Hahn.ApplicationProcess.May2020.Web.Examples {
	public class ApplicantExample : IExamplesProvider<Applicant>
	{
		public Applicant GetExamples() {
			return new Applicant {
				Name = "Alison",
				FamilyName = "Johnson",
				Address = "8776 West Schoolhouse Dr. Dalton, GA 30721",
				CountryOfOrigin = "United States of America",
				Age = 25,
				EMailAddress = "alison@johnson.com",
				Hired = null
			};
		}
	}
}
